#include <Wire.h>
#include <Kalman.h> // Source: https://github.com/TKJElectronics/KalmanFilter

#define RESTRICT_PITCH // Comment out to restrict roll to ±90deg instead - please read: http://www.freescale.com/files/sensors/doc/app_note/AN3461.pdf
#define DEBUG false

const uint8_t IMUAddress = 0x68; // AD0 is logic low on the PCB
const uint16_t I2C_TIMEOUT = 1000; // Used to check for errors in I2C communication

#define FLY 9
#define CONTROL_X 10 //좌우 조정
#define CONTROL_Y 11
								   //+는 앞 또는 우
								   //-는 뒤 또는 좌

								   //출력시 +가 오른쪽, 아래
#define ARROW A2
#define FLY_FLEX A0
#define MAX_POWER 173
typedef enum s {
	NONE = 1 << 0,
	UP = 1 << 1,
	DOWN = 1 << 2,
	LEFT = 1 << 3,
	RIGHT = 1 << 4
} GyroState;

//fly가 방향
//arrow가 

//뒤로 -70 
//좌, 우 : -는 Back기준으로 좌, +는 우
//앞, 뒤 : -는 Back기준으로 뒤, +는 앞

typedef struct cg {
	double x = 0.0, y = 0.0;
} *Gyro;

typedef struct gg {
	Gyro current;
	bool isBack = false;
	uint8_t moved;
} *Gyros;


Kalman kalmanX;
Kalman kalmanY;

/* IMU Data */
double accX, accY, accZ;
double gyroX, gyroY, gyroZ;
int16_t tempRaw;

double gyroXangle, gyroYangle; // Angle calculate using the gyro only
double compAngleX, compAngleY; // Calculated angle using a complementary filter
double kalAngleX, kalAngleY; // Calculated angle using a Kalman filter

uint32_t timer;
uint8_t i2cData[14];
Gyros gyros;

bool isArrow = false;
//ox 는 상하
//cx, cy는 컨트롤 x, y
int power = 0, ox = 0, cx = 0, cy = 0, backup = 0, change = 0;
//Function
uint8_t i2cRead(uint8_t, uint8_t *, uint8_t);
uint8_t i2cWrite(uint8_t, uint8_t *, uint8_t, bool);
uint8_t i2cWrite(uint8_t, uint8_t, bool);

void setGyroState(Gyros gy);

void setup() {
	Serial.begin(115200);
	Wire.begin();

	pinMode(FLY_FLEX, INPUT);
	pinMode(ARROW, INPUT);
	pinMode(FLY, OUTPUT);
	pinMode(CONTROL_X, OUTPUT);
	pinMode(CONTROL_Y, OUTPUT);

	analogWrite(FLY, 0);
#if ARDUINO >= 157
	Wire.setClock(400000UL); // Set I2C frequency to 400kHz
#else
	TWBR = ((F_CPU / 400000UL) - 16) / 2; // Set I2C frequency to 400kHz
#endif

	i2cData[0] = 7; // Set the sample rate to 1000Hz - 8kHz/(7+1) = 1000Hz
	i2cData[1] = 0x00; // Disable FSYNC and set 260 Hz Acc filtering, 256 Hz Gyro filtering, 8 KHz sampling
	i2cData[2] = 0x00; // Set Gyro Full Scale Range to ±250deg/s
	i2cData[3] = 0x00; // Set Accelerometer Full Scale Range to ±2g
	while (i2cWrite(0x19, i2cData, 4, false)); // Write to all four registers at once
	while (i2cWrite(0x6B, 0x01, true)); // PLL with X axis gyroscope reference and disable sleep mode

	while (i2cRead(0x75, i2cData, 1));
	if (i2cData[0] != 0x68) {
		Serial.print(F("Error reading sensor"));
		while (1);
	}

	delay(100);

	while (i2cRead(0x3B, i2cData, 6));
	accX = (i2cData[0] << 8) | i2cData[1];
	accY = (i2cData[2] << 8) | i2cData[3];
	accZ = (i2cData[4] << 8) | i2cData[5];


#ifdef RESTRICT_PITCH // Eq. 25 and 26
	double roll = atan2(accY, accZ) * RAD_TO_DEG;
	double pitch = atan(-accX / sqrt(accY * accY + accZ * accZ)) * RAD_TO_DEG;
#else // Eq. 28 and 29
	double roll = atan(accY / sqrt(accX * accX + accZ * accZ)) * RAD_TO_DEG;
	double pitch = atan2(-accX, accZ) * RAD_TO_DEG;
#endif

	kalmanX.setAngle(roll);
	kalmanY.setAngle(pitch);
	gyroXangle = roll;
	gyroYangle = pitch;
	compAngleX = roll;
	compAngleY = pitch;
	timer = micros();
	analogWrite(FLY, MAX_POWER);
	ox = MAX_POWER;
}

void loop() {
	while (i2cRead(0x3B, i2cData, 14));
	accX = ((i2cData[0] << 8) | i2cData[1]);
	accY = ((i2cData[2] << 8) | i2cData[3]);
	accZ = ((i2cData[4] << 8) | i2cData[5]);
	tempRaw = (i2cData[6] << 8) | i2cData[7];
	gyroX = (i2cData[8] << 8) | i2cData[9];
	gyroY = (i2cData[10] << 8) | i2cData[11];
	gyroZ = (i2cData[12] << 8) | i2cData[13];

	double dt = (double)(micros() - timer) / 1000000;
	timer = micros();


	// Source: http://www.freescale.com/files/sensors/doc/app_note/AN3461.pdf eq. 25 and eq. 26
	// atan2 outputs the value of -π to π (radians) - see http://en.wikipedia.org/wiki/Atan2
	// It is then converted from radians to degrees
#ifdef RESTRICT_PITCH // Eq. 25 and 26
	double roll = atan2(accY, accZ) * RAD_TO_DEG;
	double pitch = atan(-accX / sqrt(accY * accY + accZ * accZ)) * RAD_TO_DEG;
#else // Eq. 28 and 29
	double roll = atan(accY / sqrt(accX * accX + accZ * accZ)) * RAD_TO_DEG;
	double pitch = atan2(-accX, accZ) * RAD_TO_DEG;
#endif

	double gyroXrate = gyroX / 131.0; // Convert to deg/s`
	double gyroYrate = gyroY / 131.0; // Convert to deg/s

#ifdef RESTRICT_PITCH
									  // This fixes the transition problem when the accelerometer angle jumps between -180 and 180 degrees
	if ((roll < -90 && kalAngleX > 90) || (roll > 90 && kalAngleX < -90)) {
		kalmanX.setAngle(roll);
		compAngleX = roll;
		kalAngleX = roll;
		gyroXangle = roll;
	}
	else
		kalAngleX = kalmanX.getAngle(roll, gyroXrate, dt); // Calculate the angle using a Kalman filter

	if (abs(kalAngleX) > 90)
		gyroYrate = -gyroYrate; // Invert rate, so it fits the restriced accelerometer reading
	kalAngleY = kalmanY.getAngle(pitch, gyroYrate, dt);
#else
									  // This fixes the transiti

	on problem when the accelerometer angle jumps between - 180 and 180 degrees
		if ((pitch < -90 && kalAngleY > 90) || (pitch > 90 && kalAngleY < -90)) {
			kalmanY.setAngle(pitch);
			compAngleY = pitch;
			kalAngleY = pitch;
			gyroYangle = pitch;
		}
		else
			kalAngleY = kalmanY.getAngle(pitch, gyroYrate, dt); // Calculate the angle using a Kalman filter

	if (abs(kalAngleY) > 90)
		gyroXrate = -gyroXrate; // Invert rate, so it fits the restriced accelerometer reading
	kalAngleX = kalmanX.getAngle(roll, gyroXrate, dt); // Calculate the angle using a Kalman filter
#endif

	gyroXangle += gyroXrate * dt;
	gyroYangle += gyroYrate * dt;

	compAngleX = 0.93 * (compAngleX + gyroXrate * dt) + 0.07 * roll;
	compAngleY = 0.93 * (compAngleY + gyroYrate * dt) + 0.07 * pitch;

	if (gyroXangle < -180 || gyroXangle > 180)
		gyroXangle = kalAngleX;
	if (gyroYangle < -180 || gyroYangle > 180)
		gyroYangle = kalAngleY;

#if 0
	Serial.print(accX); Serial.print("\t");
	Serial.print(accY); Serial.print("\t");
	Serial.print(accZ); Serial.print("\t");

	Serial.print(gyroX); Serial.print("\t");
	Serial.print(gyroY); Serial.print("\t");
	Serial.print(gyroZ); Serial.print("\t");

	Serial.print("\t");
#endif

#if DEBUG
	Serial.print(roll); Serial.print("\t");
	Serial.print(gyroXangle); Serial.print("\t");
	Serial.print(compAngleX); Serial.print("\t");
	Serial.print(kalAngleX); Serial.print("\t");

	Serial.print("\t");

	Serial.print(pitch); Serial.print("\t");
	Serial.print(gyroYangle); Serial.print("\t");
	Serial.print(compAngleY); Serial.print("\t");
	Serial.print(kalAngleY); Serial.print("\t");
#endif

	gyros->current->x = kalAngleX;
	gyros->current->y = kalAngleY;
	setGyroState(gyros);
	//Serial.print(kalAngleY);  Serial.print("\t");
	cx = cy = 0;
	if (analogRead(ARROW) > 870) {
		Serial.print("CONTROL "); Serial.print("\t");
		if (gyros->isBack) {
			cx = (int)((kalAngleX + 90) / 1.04);
			cy = (int)((kalAngleY + 90) / 1.04);
		}
	}
	else {
		int a = analogRead(FLY_FLEX);
		change = backup - a;
		backup = a;
		if (backup > 650) {
			change /= 20;
			Serial.print(String(change)); Serial.print("\t");
			if (change < 0) {
				power += gyros->isBack ? change : abs(change);
				if (power > 100)
					power = 100;
				else if (power < 0)
					power = 0;
			}
			ox = 173 - ((int)(power * 1.73));
		}
	}
	cx = constrain(cx, 0, MAX_POWER);
	cy = constrain(cy, 0, MAX_POWER);
	ox = constrain(ox, 0, MAX_POWER);
	if (!(ox > 0) | !(ox < 173))
		ox = 173;
	Serial.print(ox); Serial.print("\t");
	Serial.print(cx); Serial.print("\t");
	Serial.print(cy); Serial.println();
	analogWrite(FLY, ox);
	analogWrite(CONTROL_X, cx);
	analogWrite(CONTROL_Y, cy);
#if 0
	Serial.print("\t");
	double temperature = (double)tempRaw / 340.0 + 36.53;
	Serial.print(temperature); Serial.print("\t");
#endif
	delay(100);
}

void setGyroState(Gyros gy) {
	gy->isBack = (abs(gy->current->x) < 90);
	//Serial.print(gy->current->x); Serial.print("\t");
	//Serial.print(gy->isBack ? "Back\t" : "Front\t");
}

uint8_t i2cWrite(uint8_t registerAddress, uint8_t data, bool sendStop) {
	return i2cWrite(registerAddress, &data, 1, sendStop);
}

uint8_t i2cWrite(uint8_t registerAddress, uint8_t *data, uint8_t length, bool sendStop) {
	Wire.beginTransmission(IMUAddress);
	Wire.write(registerAddress);
	Wire.write(data, length);
	uint8_t rcode = Wire.endTransmission(sendStop);
	if (rcode) {
		Serial.print(F("i2cWrite failed: "));
		Serial.println(rcode);
	}
	return rcode;
}

uint8_t i2cRead(uint8_t registerAddress, uint8_t *data, uint8_t nbytes) {
	uint32_t timeOutTimer;
	Wire.beginTransmission(IMUAddress);
	Wire.write(registerAddress);
	uint8_t rcode = Wire.endTransmission(false);
	if (rcode) {
		Serial.print(F("i2cRead failed: "));
		Serial.println(rcode);
		return rcode;
	}
	Wire.requestFrom(IMUAddress, nbytes, (uint8_t)true);
	for (uint8_t i = 0; i < nbytes; i++) {
		if (Wire.available())
			data[i] = Wire.read();
		else {
			timeOutTimer = micros();
			while (((micros() - timeOutTimer) < I2C_TIMEOUT) && !Wire.available());
			if (Wire.available())
				data[i] = Wire.read();
			else {
				Serial.println(F("i2cRead timeout"));
				return 5;
			}
		}
	}
	return 0;
}